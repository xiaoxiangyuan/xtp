package cn.xtits.xtp.service;

import cn.xtits.xtp.entity.FormConfig;
import cn.xtits.xtp.entity.FormConfigExample;

import java.util.List;
import java.util.Map;

/**
 * Created by Dan on 2018-05-11 10:31:52
 */
public interface FormConfigService {

    Integer listFormKeyByFormConfigCount(Map<String, Object> map);

    List<FormConfig> listFormKeyByFormConfig(Map<String, Object> map);

    int deleteByPrimaryKey(Integer id);

    int insert(FormConfig record);

    List<FormConfig> listByExample(FormConfigExample example);

    FormConfig getByPrimaryKey(Integer id);

    int updateByPrimaryKey(FormConfig record);

    int updateByPrimaryKeySelective(FormConfig record);

}