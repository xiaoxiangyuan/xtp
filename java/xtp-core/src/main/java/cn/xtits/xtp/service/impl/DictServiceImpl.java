
package cn.xtits.xtp.service.impl;

import cn.xtits.xtp.entity.Dict;
import cn.xtits.xtp.entity.DictExample;
import cn.xtits.xtp.mapper.base.DictMapper;
import cn.xtits.xtp.service.DictService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by ShengHaiJiang on 2017/3/7.
 */
@Service
public class DictServiceImpl implements DictService {

    @Resource
    private DictMapper mapper;


    @Override
    public int deleteByPrimaryKey(Integer id) {
        return mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(Dict record) {
        return mapper.insertSelective(record);
    }

    @Override
    public List<Dict> listByExample(DictExample example) {
        PageHelper.startPage(example.getPageIndex().intValue(), example.getPageSize().intValue());
        Page page = (Page) mapper.selectByExample(example);
        example.setCount((int) page.getTotal());
        return page.toPageInfo().getList();
    }

    @Override
    public Dict getByPrimaryKey(Integer id) {
        return mapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKey(Dict record) {
        return mapper.updateByPrimaryKey(record);
    }

    @Override
    public int updateByPrimaryKeySelective(Dict record) {
        return mapper.updateByPrimaryKeySelective(record);
    }
}