package cn.xtits.xtp.service.impl;
import cn.xtits.xtp.entity.SystemRoleConfig;
import cn.xtits.xtp.entity.SystemRoleConfigExample;
import cn.xtits.xtp.mapper.base.SystemRoleConfigMapper;
import cn.xtits.xtp.service.SystemRoleConfigService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Dan on 2018-03-22 10:34:08
 */
@Service
public class SystemRoleConfigServiceImpl implements SystemRoleConfigService {

    @Resource
    private SystemRoleConfigMapper systemRoleConfigMapper;

    @Override
    public int deleteByPrimaryKey(Integer id) {
        return systemRoleConfigMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(SystemRoleConfig record) {
        return systemRoleConfigMapper.insertSelective(record);
    }

    @Override
    public List<SystemRoleConfig> listByExample(SystemRoleConfigExample example) {
        PageHelper.startPage(example.getPageIndex().intValue(), example.getPageSize().intValue());
        Page page = (Page) systemRoleConfigMapper.selectByExample(example);
        example.setCount((int)page.getTotal());
        return page.toPageInfo().getList();
    }

    @Override
    public SystemRoleConfig getByPrimaryKey(Integer id) {
        return systemRoleConfigMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKey(SystemRoleConfig record) {
        return systemRoleConfigMapper.updateByPrimaryKey(record);
    }
    
    @Override
    public int updateByPrimaryKeySelective(SystemRoleConfig record) {
        return systemRoleConfigMapper.updateByPrimaryKeySelective(record);
    }



}