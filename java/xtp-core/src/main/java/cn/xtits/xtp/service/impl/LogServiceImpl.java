
package cn.xtits.xtp.service.impl;

import cn.xtits.xtp.entity.Log;
import cn.xtits.xtp.entity.LogExample;
import cn.xtits.xtp.mapper.base.LogMapper;
import cn.xtits.xtp.service.LogService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by ShengHaiJiang on 2017/3/7.
 */
@Service
public class LogServiceImpl implements LogService {

    @Resource
    private LogMapper logMapper;


    @Override
    public int deleteByPrimaryKey(Integer ID) {
        return logMapper.deleteByPrimaryKey(ID);
    }

    @Override
    public int insert(Log record) {
        return logMapper.insert(record);
    }

    @Override
    public List<Log> listByExample(LogExample example) {
        PageHelper.startPage(example.getPageIndex().intValue(), example.getPageSize().intValue());
        Page page = (Page) logMapper.selectByExample(example);
        example.setCount((int) page.getTotal());
        return page.toPageInfo().getList();
    }

    @Override
    public Log getByPrimaryKey(Integer ID) {
        return logMapper.selectByPrimaryKey(ID);
    }

    @Override
    public int updateByPrimaryKey(Log record) {
        return logMapper.updateByPrimaryKey(record);
    }
}