package cn.xtits.xtp.service.impl;

import cn.xtits.xtp.entity.FormConfig;
import cn.xtits.xtp.entity.FormConfigExample;
import cn.xtits.xtp.mapper.base.FormConfigMapper;
import cn.xtits.xtp.service.FormConfigService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * Created by Dan on 2018-05-11 10:31:52
 */
@Service
public class FormConfigServiceImpl implements FormConfigService {

    @Resource
    private FormConfigMapper formConfigMapper;

    @Override
    public Integer listFormKeyByFormConfigCount(Map<String, Object> map) {
        return formConfigMapper.listFormKeyByFormConfigCount(map);
    }

    @Override
    public List<FormConfig> listFormKeyByFormConfig(Map<String, Object> map) {
        return formConfigMapper.listFormKeyByFormConfig(map);
    }

    @Override
    public int deleteByPrimaryKey(Integer id) {
        return formConfigMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(FormConfig record) {
        return formConfigMapper.insertSelective(record);
    }

    @Override
    public List<FormConfig> listByExample(FormConfigExample example) {
        PageHelper.startPage(example.getPageIndex().intValue(), example.getPageSize().intValue());
        Page page = (Page) formConfigMapper.selectByExample(example);
        example.setCount((int) page.getTotal());
        return page.toPageInfo().getList();
    }

    @Override
    public FormConfig getByPrimaryKey(Integer id) {
        return formConfigMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKey(FormConfig record) {
        return formConfigMapper.updateByPrimaryKey(record);
    }

    @Override
    public int updateByPrimaryKeySelective(FormConfig record) {
        return formConfigMapper.updateByPrimaryKeySelective(record);
    }


}