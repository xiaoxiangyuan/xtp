
package cn.xtits.xtp.service.impl;

import cn.xtits.xtp.entity.RoleMenuOperation;
import cn.xtits.xtp.entity.RoleMenuOperationExample;
import cn.xtits.xtp.mapper.base.RoleMenuOperationMapper;
import cn.xtits.xtp.service.RoleMenuOperationService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by ShengHaiJiang on 2017/3/7.
 */
@Service
public class RoleMenuOperationServiceImpl implements RoleMenuOperationService {

    @Resource
    private RoleMenuOperationMapper roleMenuOperationMapper;


    @Override
    public int deleteByPrimaryKey(Integer ID) {
        return roleMenuOperationMapper.deleteByPrimaryKey(ID);
    }

    @Override
    public int insert(RoleMenuOperation record) {
        return roleMenuOperationMapper.insert(record);
    }

    @Override
    public List<RoleMenuOperation> listByExample(RoleMenuOperationExample example) {
        PageHelper.startPage(example.getPageIndex().intValue(), example.getPageSize().intValue());
        Page page = (Page) roleMenuOperationMapper.selectByExample(example);
        example.setCount((int) page.getTotal());
        return page.toPageInfo().getList();
    }

    @Override
    public RoleMenuOperation getByPrimaryKey(Integer ID) {
        return roleMenuOperationMapper.selectByPrimaryKey(ID);
    }

    @Override
    public int updateByPrimaryKey(RoleMenuOperation record) {
        return roleMenuOperationMapper.updateByPrimaryKey(record);
    }

    @Override
    public int updateRoleMenuOperation(Integer roleId, List<RoleMenuOperation> roleMenuOperations) {
        RoleMenuOperationExample example = new RoleMenuOperationExample();
        RoleMenuOperationExample.Criteria criteria = example.createCriteria();
        criteria.andRoleIdEqualTo(roleId);
        List<RoleMenuOperation> list = roleMenuOperationMapper.selectByExample(example);
        for (RoleMenuOperation userMenuOperation : list) {
            roleMenuOperationMapper.deleteByPrimaryKey(userMenuOperation.getId());
        }
        for (RoleMenuOperation userMenu : roleMenuOperations) {
            userMenu.setRoleId(roleId);
            roleMenuOperationMapper.insert(userMenu);
        }
        return 1;
    }
}