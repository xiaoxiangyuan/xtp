package cn.xtits.xtp.service;

import cn.xtits.xtp.entity.Warehouse;
import cn.xtits.xtp.entity.WarehouseExample;
import java.util.List;

/**
 * Created by 
 */
public interface WarehouseService {

    int deleteByPrimaryKey(Integer id);

    int insert(Warehouse record);

    List<Warehouse> listByExample(WarehouseExample example);

    Warehouse getByPrimaryKey(Integer id);

    int updateByPrimaryKey(Warehouse record);
    
    int updateByPrimaryKeySelective(Warehouse record);
}