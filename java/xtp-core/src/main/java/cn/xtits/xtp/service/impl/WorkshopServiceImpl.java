package cn.xtits.xtp.service.impl;
import cn.xtits.xtp.entity.Workshop;
import cn.xtits.xtp.entity.WorkshopExample;
import cn.xtits.xtp.mapper.base.WorkshopMapper;
import cn.xtits.xtp.service.WorkshopService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Generator on 2018-08-21 09:16:58
 */
@Service
public class WorkshopServiceImpl implements WorkshopService {

    @Resource
    private WorkshopMapper workshopMapper;

    @Override
    public int deleteByPrimaryKey(Integer id) {
        return workshopMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(Workshop record) {
        return workshopMapper.insertSelective(record);
    }

    @Override
    public List<Workshop> listByExample(WorkshopExample example) {
        PageHelper.startPage(example.getPageIndex().intValue(), example.getPageSize().intValue());
        Page page = (Page) workshopMapper.selectByExample(example);
        example.setCount((int)page.getTotal());
        return page.toPageInfo().getList();
    }

    @Override
    public Workshop getByPrimaryKey(Integer id) {
        return workshopMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKey(Workshop record) {
        return workshopMapper.updateByPrimaryKey(record);
    }
    
    @Override
    public int updateByPrimaryKeySelective(Workshop record) {
        return workshopMapper.updateByPrimaryKeySelective(record);
    }



}