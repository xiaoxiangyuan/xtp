package cn.xtits.xtp.service.impl;

import cn.xtits.xtp.entity.Supplier;
import cn.xtits.xtp.entity.SupplierExample;
import cn.xtits.xtp.mapper.base.SupplierMapper;
import cn.xtits.xtp.service.SupplierService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Dan on 2017-12-08 10:37:10
 */
@Service
public class SupplierServiceImpl implements SupplierService {

    @Resource
    private SupplierMapper mapper;


    @Override
    public int deleteByPrimaryKey(Integer id) {
        return mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(Supplier record) {
        return mapper.insertSelective(record);
    }

    @Override
    public List<Supplier> listByExample(SupplierExample example) {
        PageHelper.startPage(example.getPageIndex().intValue(), example.getPageSize().intValue());
        Page page = (Page) mapper.selectByExample(example);
        example.setCount((int)page.getTotal());
        return page.toPageInfo().getList();
    }

    @Override
    public Supplier getByPrimaryKey(Integer id) {
        return mapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKey(Supplier record) {
        return mapper.updateByPrimaryKey(record);
    }
    
    @Override
    public int updateByPrimaryKeySelective(Supplier record) {
        return mapper.updateByPrimaryKeySelective(record);
    }
}