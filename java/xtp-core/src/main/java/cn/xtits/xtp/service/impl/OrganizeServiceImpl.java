
package cn.xtits.xtp.service.impl;

import cn.xtits.xtp.entity.Organize;
import cn.xtits.xtp.entity.OrganizeExample;
import cn.xtits.xtp.mapper.base.OrganizeMapper;
import cn.xtits.xtp.service.OrganizeService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by ShengHaiJiang on 2017/3/7.
 */
@Service
public class OrganizeServiceImpl implements OrganizeService {

    @Resource
    private OrganizeMapper organizeMapper;

    @Override
    public int deleteByPrimaryKey(Integer id) {
        return organizeMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(Organize record) {
        return organizeMapper.insert(record);
    }

    @Override
    public List<Organize> listByExample(OrganizeExample example) {
        PageHelper.startPage(example.getPageIndex().intValue(), example.getPageSize().intValue());
        Page page = (Page) organizeMapper.selectByExample(example);
        example.setCount((int) page.getTotal());
        return page.toPageInfo().getList();
    }

    @Override
    public Organize getByPrimaryKey(Integer id) {
        return organizeMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKey(Organize record) {
        return organizeMapper.updateByPrimaryKey(record);
    }

    @Override
    public int updateByPrimaryKeySelective(Organize record) {
        return organizeMapper.updateByPrimaryKeySelective(record);
    }

}