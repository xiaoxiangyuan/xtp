package cn.xtits.xtp.controller;

import cn.xtits.xtf.common.utils.JsonUtil;
import cn.xtits.xtf.common.web.AjaxResult;
import cn.xtits.xtf.common.web.Pagination;
import cn.xtits.xtp.entity.SystemRoleConfig;
import cn.xtits.xtp.entity.SystemRoleConfigExample;
import cn.xtits.xtp.service.SystemRoleConfigService;
import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @fileName: SystemRoleConfigController.java
 * @author: Dan
 * @createDate: 2018-03-22 10:34:07
 * @description: 系统参数配置
 */
@RestController
@RequestMapping("/systemRoleConfig")
public class SystemRoleConfigController extends BaseController {

    @Autowired
    private SystemRoleConfigService systemRoleConfigService;

    @RequestMapping(value = "insertSystemRoleConfig")
    public AjaxResult insertSystemRoleConfig(
            @RequestParam(value = "data", required = false) String data) {
        SystemRoleConfig record = JsonUtil.fromJson(data, SystemRoleConfig.class);
        //Date dt = getDateNow();
        record.setCreateDate(null);
        record.setMakeBillMan(getUserName());
        record.setModifier(getUserName());
        record.setModifyDate(null);
        record.setDeleteFlag(false);
        systemRoleConfigService.insert(record);
        return new AjaxResult(record);
    }

    @RequestMapping(value = "deleteSystemRoleConfig")
    public AjaxResult deleteSystemRoleConfig(
            @RequestParam(value = "id", required = false) int id) {
        SystemRoleConfig record = new SystemRoleConfig();
        record.setId(id);
        record.setDeleteFlag(true);
        record.setModifier(getUserName());
        record.setModifyDate(null);
        int row = systemRoleConfigService.updateByPrimaryKeySelective(record);
        return new AjaxResult(row);
    }

    @RequestMapping(value = "updateSystemRoleConfig")
    public AjaxResult updateSystemRoleConfig(
            @RequestParam(value = "data", required = false) String data) {
        SystemRoleConfig record = JsonUtil.fromJson(data, SystemRoleConfig.class);
        record.setCreateDate(null);
        record.setMakeBillMan(null);
        record.setModifyDate(null);
        record.setModifier(getUserName());
        record.setDeleteFlag(false);
        systemRoleConfigService.updateByPrimaryKeySelective(record);
        return new AjaxResult(record);
    }

    @RequestMapping(value = "listSystemRoleConfig")
    public AjaxResult listSystemRoleConfig(
            @RequestParam(value = "name", required = false) String name,
            @RequestParam(value = "type", required = false) String type,
            @RequestParam(value = "key", required = false) String key,
            @RequestParam(value = "value", required = false) String value,
            @RequestParam(value = "remark", required = false) String remark,
            @RequestParam(value = "keyFlag", required = false) Boolean keyFlag,
            @RequestParam(value = "pageSize", required = false) Integer pageSize,
            @RequestParam(value = "pageIndex", required = false) Integer pageIndex,
            @RequestParam(value = "orderBy", required = false) String orderBy,
            @RequestParam(value = "startDate", required = false) String startDate,
            @RequestParam(value = "endDate", required = false) String endDate) {
        DateTimeFormatter format = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
        SystemRoleConfigExample example = new SystemRoleConfigExample();
        example.setPageIndex(pageIndex);
        example.setPageSize(pageSize);
        if (StringUtils.isNotBlank(orderBy)) {
            example.setOrderByClause(orderBy);
        }
        SystemRoleConfigExample.Criteria criteria = example.createCriteria();

        criteria.andDeleteFlagEqualTo(false);
        if (StringUtils.isNotBlank(name)) {
            criteria.andNameLike(name);
        }
        if (StringUtils.isNotBlank(type)) {
            criteria.andTypeLike(type);
        }
        if (StringUtils.isNotBlank(key)) {
            criteria.andKeyLike(key);
        }
        if (StringUtils.isNotBlank(value)) {
            criteria.andValueLike(value);
        }
        if (StringUtils.isNotBlank(remark)) {
            criteria.andRemarkLike(remark);
        }
        if (keyFlag != null) {
            criteria.andKeyFlagEqualTo(keyFlag);
        }
        if (StringUtils.isNotBlank(startDate)) {
            criteria.andCreateDateGreaterThanOrEqualTo(DateTime.parse(startDate, format).toDate());
        }
        if (StringUtils.isNotBlank(endDate)) {

            criteria.andCreateDateLessThanOrEqualTo(DateTime.parse(endDate, format).toDate());
        }

        List<SystemRoleConfig> list = systemRoleConfigService.listByExample(example);
        Pagination<SystemRoleConfig> pList = new Pagination<>(example, list, example.getCount());
        return new AjaxResult(pList);
    }

    @RequestMapping(value = "getSystemRoleConfig")
    public AjaxResult getSystemRoleConfig(
            @RequestParam(value = "id", required = false) Integer id) {
        SystemRoleConfig res = systemRoleConfigService.getByPrimaryKey(id);
        return new AjaxResult(res);
    }


}