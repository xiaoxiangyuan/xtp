package cn.xtits.xtp.dto;

import cn.xtits.xtp.entity.Area;

import java.util.ArrayList;
import java.util.List;

/**
 * @fileName: AreaDto
 * @author: Dan
 * @createDate: 2018-03-28 14:22.
 * @description:
 */
public class AreaDto extends Area {

    List<AreaDto> children = new ArrayList<>();

    public List<AreaDto> getChildren() {
        return children;
    }

    public void setChildren(List<AreaDto> children) {
        this.children = children;
    }
}
