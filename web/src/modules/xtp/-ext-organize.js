import {OrganizeAPI} from "./base";
import {XTP} from "../api";

Object.assign(OrganizeAPI, {
  getOrganizeCoding(params) {
    return new Promise((resolve) => {
      XTP.organize.getOrganizeCoding(params).then(({data, res}) => {
        resolve({ data, res });
      });
    });
  },
  listOrganize(params) {
    return new Promise((resolve) => {
      XTP.organize.listOrganize(params).then(({data, res}) => {
        let list = [];
        if (data.code === 1) {list = data.data.data;}
        list.map((element) => {
          element.isEdit = false;
          element.children = [];
          element.level = element.level + "";
        });
        resolve({ data, list, res });
      });
    });
  },
  listOrganizeTree(params) {
    return new Promise((resolve) => {
      XTP.organize.listOrganizeTree(params).then(({data, res}) => {
        const childrenFun = function(item) {
          item.label = item.fullName;
          if (item.children) {
            item.children.map((child) => {
              child.label = child.fullName;
              childrenFun(child);
            });
            if (item.children.length === 0) {delete item.children;}
          }
        };
        let list = [];
        if (data.code === 1) {list = data.data;}
        list.map((element) => {
          childrenFun(element);
          element.isEdit = false;
        });
        resolve({data, list});
      });
    });
  }
});

export {
  OrganizeAPI
};
