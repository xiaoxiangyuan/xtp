import { IBASIC } from "../../api";
import { Message } from "element-ui";

const Supplier = function() {
  return {
    id: 0,
    code: null,
    name: null,
    shortName: null,
    director: null,
    tel: null,
    phone: null,
    address: null,
    taxId: null,
    mail: null,
    site: null,
    fax: null,
    depositBank: null,
    bankAccount: null,
    credit: null,
    remark: null
  };
},
SupplierAPI = {
  supplier: new Supplier(),
  get: () => SupplierAPI.supplier,
  set: (value) => {
    SupplierAPI.supplier = Object.assign(SupplierAPI.supplier, value);
  },
  init: () => new Supplier(),
  getSupplier(params) {
    return new Promise((resolve) => {
      IBASIC.supplier.getSupplier(params).then(({data, res}) => {
        let item = {};
        if (data.code === 1) {item = data.data;}
        resolve({ data, item, res });
        });
    });
  },
  listSupplier(params) {
    return new Promise((resolve) => {
      IBASIC.supplier.listSupplier(params).then(({data, res}) => {
        let list = [];
        if (data.code === 1) {list = data.data.data;}
        resolve({ data, list, res });
        });
    });
  },
  insertSupplier(params) {
    return new Promise((resolve) => {
      IBASIC.supplier.insertSupplier(params).then(({data, res}) => {
        if (data.code === 1) {
          Message({
            message: "新增成功",
            type: "success"
          });
        }
        resolve({data, res});
        });
    });
  },
  updateSupplier(params) {
    return new Promise((resolve) => {
      IBASIC.supplier.updateSupplier(params).then(({data, res}) => {
        if (data.code === 1) {
          Message({
            message: "修改成功",
            type: "success"
          });
        }
        resolve({data, res});
        });
    });
  },
  deleteSupplier(params) {
    return new Promise((resolve) => {
      IBASIC.supplier.deleteSupplier(params).then(({data, res}) => {
        if (data.code === 1) {
          Message({
            message: "删除成功",
            type: "success"
          });
        }
        resolve({data, res});
        });
    });
  }
};

export { SupplierAPI };
