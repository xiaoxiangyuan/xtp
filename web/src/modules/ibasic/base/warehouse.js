import { IBASIC } from "../../api";
import { Message } from "element-ui";

const Warehouse = function() {
  return {
    id: 0,
    code: null,
    name: null,
    location: null,
    manager: null,
    status: null
  };
},
WarehouseAPI = {
  warehouse: new Warehouse(),
  get: () => WarehouseAPI.warehouse,
  set: (value) => {
    WarehouseAPI.warehouse = Object.assign(WarehouseAPI.warehouse, value);
  },
  init: () => new Warehouse(),
  getWarehouse(params) {
    return new Promise((resolve) => {
      IBASIC.warehouse.getWarehouse(params).then(({data, res}) => {
        let item = {};
        if (data.code === 1) {item = data.data;}
        resolve({ data, item, res });
        });
    });
  },
  listWarehouse(params) {
    return new Promise((resolve) => {
      IBASIC.warehouse.listWarehouse(params).then(({data, res}) => {
        let list = [];
        if (data.code === 1) {list = data.data.data;}
        resolve({ data, list, res });
        });
    });
  },
  insertWarehouse(params) {
    return new Promise((resolve) => {
      IBASIC.warehouse.insertWarehouse(params).then(({data, res}) => {
        if (data.code === 1) {
          Message({
            message: "新增成功",
            type: "success"
          });
        }
        resolve({data, res});
        });
    });
  },
  updateWarehouse(params) {
    return new Promise((resolve) => {
      IBASIC.warehouse.updateWarehouse(params).then(({data, res}) => {
        if (data.code === 1) {
          Message({
            message: "修改成功",
            type: "success"
          });
        }
        resolve({data, res});
        });
    });
  },
  deleteWarehouse(params) {
    return new Promise((resolve) => {
      IBASIC.warehouse.deleteWarehouse(params).then(({data, res}) => {
        if (data.code === 1) {
          Message({
            message: "删除成功",
            type: "success"
          });
        }
        resolve({data, res});
        });
    });
  }
};

export { WarehouseAPI };
