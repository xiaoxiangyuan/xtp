import { IBASIC } from "../../api";
import { Message } from "element-ui";

const Area = function() {
  return {
    id: 0,
    parentId: null,
    code: null,
    name: null,
    level: null,
    leafFlag: null
  };
},
AreaAPI = {
  area: new Area(),
  get: () => AreaAPI.area,
  set: (value) => {
    AreaAPI.area = Object.assign(AreaAPI.area, value);
  },
  init: () => new Area(),
  getArea(params) {
    return new Promise((resolve) => {
      IBASIC.area.getArea(params).then(({data, res}) => {
        let item = {};
        if (data.code === 1) {item = data.data;}
        resolve({ data, item, res });
        });
    });
  },
  listArea(params) {
    return new Promise((resolve) => {
      IBASIC.area.listArea(params).then(({data, res}) => {
        let list = [];
        if (data.code === 1) {list = data.data.data;}
        resolve({ data, list, res });
        });
    });
  },
  insertArea(params) {
    return new Promise((resolve) => {
      IBASIC.area.insertArea(params).then(({data, res}) => {
        if (data.code === 1) {
          Message({
            message: "新增成功",
            type: "success"
          });
        }
        resolve({data, res});
        });
    });
  },
  updateArea(params) {
    return new Promise((resolve) => {
      IBASIC.area.updateArea(params).then(({data, res}) => {
        if (data.code === 1) {
          Message({
            message: "修改成功",
            type: "success"
          });
        }
        resolve({data, res});
        });
    });
  },
  deleteArea(params) {
    return new Promise((resolve) => {
      IBASIC.area.deleteArea(params).then(({data, res}) => {
        if (data.code === 1) {
          Message({
            message: "删除成功",
            type: "success"
          });
        }
        resolve({data, res});
        });
    });
  }
};

export { AreaAPI };
